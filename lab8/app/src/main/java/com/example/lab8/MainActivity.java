package com.example.lab8;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void intentServ(View view) {
        Intent intent = new Intent(MainActivity.this, First.class);
        startActivity(intent);
    }

    public void sec(View view) {
        Intent intent = new Intent(MainActivity.this, Sec.class);
        startActivity(intent);
    }
}