package com.example.lab8;

import android.app.IntentService;
import android.content.Intent;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.text.format.DateFormat;


public class Timer extends IntentService {
    public static final String TEXT_INPUT = "inText";
    public static final String TEXT_OUTPUT = "outText";

    public Timer() {
        super("Timer");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {


        String inputText = intent.getStringExtra(TEXT_INPUT);
        SystemClock.sleep(3000); // 3 seconds
        String outputText = inputText + " "
                + DateFormat.format("dd/MM/yy h:mm:ss aa",
                System.currentTimeMillis());

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(
                First.ResponseReceiver.LOCAL_ACTION);
        broadcastIntent.putExtra(TEXT_OUTPUT, outputText);
        LocalBroadcastManager localBroadcastManager
                = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.sendBroadcast(broadcastIntent);

    }

}
