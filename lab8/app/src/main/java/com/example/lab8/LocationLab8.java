package com.example.lab8;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;

public class LocationLab8 extends Service{
    public LocationLab8() {
    }
    private static double distanceMeters;
    private static android.location.Location lastLocation = null;

    public final IBinder binder = new OdometerBinder();

    public class OdometerBinder extends Binder {
        LocationLab8 getOdometer() {
            return LocationLab8.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onCreate() {
        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(android.location.Location location) {
                if (location == null) {
                    lastLocation = location;
                }
                distanceMeters += location.distanceTo(lastLocation);
                lastLocation = location;

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000,
                1, locationListener);
    }
    public double getDistance(){
        return this.distanceMeters/1000;
    }
}
