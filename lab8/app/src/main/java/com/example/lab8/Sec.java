package com.example.lab8;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Sec extends AppCompatActivity {

    private LocationLab8 locationLab8;
    private boolean dound = false;

    private ServiceConnection serviceConnection= new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocationLab8.OdometerBinder odometerBinder = (LocationLab8.OdometerBinder) service;
            locationLab8 = odometerBinder.getOdometer();
            dound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

            dound = false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sec);
        watchMileage();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, com.example.lab8.LocationLab8.class);
        bindService(intent,serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (dound){
            unbindService(serviceConnection);
            dound = false;
        }
    }
    private void watchMileage(){
        final TextView distanceView = (TextView) findViewById(R.id.distance);
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                double distance = 0.0;
                if (locationLab8!=null){
                    distance=locationLab8.getDistance();
                }
                String distanceStr = String.format("1%,.2f kilometra",distance);
                distanceView.setText(distanceStr);
                handler.postDelayed(this,1000);
            }
        });
    }
}
