package com.example.lab6;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DrinkSQLHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "Drink";
    private static final int DB_VERSION = 3;


    public DrinkSQLHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        updateMyDatabase(db,0,DB_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        updateMyDatabase(db, oldVersion,newVersion);
    }

    private void updateMyDatabase(SQLiteDatabase db, int oldVersion, int newVersion){
        if(oldVersion<1){
            db.execSQL("create table DRINK( _id integer primary key autoincrement, " +
                    "NAME text, DESCR text);");
            insert(db, "kawa","kawa mniam");
            insert(db, "herbata","herbata mniam");
            insert(db, "woda","woda mniam");
        }
    }

    public void insert(SQLiteDatabase db, String name, String desc){
        ContentValues values = new ContentValues();
        values.put("NAME", name);
        values.put("DESCR",desc);
        db.insert("DRINK",null, values);

    }
}
