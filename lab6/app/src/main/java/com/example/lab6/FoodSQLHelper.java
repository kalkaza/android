package com.example.lab6;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class FoodSQLHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "Food";
    private static final int DB_VERSION = 3;


    public FoodSQLHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        updateMyDatabase(db,0,DB_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        updateMyDatabase(db, oldVersion,newVersion);
    }

    private void updateMyDatabase(SQLiteDatabase db, int oldVersion, int newVersion){
        if(oldVersion<1){
            db.execSQL("create table FOOD( _id integer primary key autoincrement, " +
                    "NAME text, DESCR text);");
            insert(db, "kotlet schabowy","schabowy taki dobry mniam");
            insert(db, "golonka","golonka taka dobra mniam");
            insert(db, "zupa ogórkowa","ogórkowa taka dobra zupka mniam");
        }
    }

    public void insert(SQLiteDatabase db, String name, String desc){
        ContentValues values = new ContentValues();
        values.put("NAME", name);
        values.put("DESCR",desc);
        db.insert("FOOD",null, values);

    }
}
