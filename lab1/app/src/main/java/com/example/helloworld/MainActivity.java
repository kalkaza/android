package com.example.helloworld;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void suma(View view){
        TextView wynik = (TextView) findViewById(R.id.textView6);
        Spinner l1 = (Spinner) findViewById(R.id.spinner3);
        int var1=Integer.parseInt(String.valueOf(l1.getSelectedItem()));
        Spinner l2 = (Spinner) findViewById(R.id.spinner);
        int var2=Integer.parseInt(String.valueOf(l2.getSelectedItem()));
        int sum = var1+var2;

        wynik.setText(Integer.toString(sum));
    }
    public void różnica(View view){
        TextView wynik = (TextView) findViewById(R.id.textView6);
        Spinner l1 = (Spinner) findViewById(R.id.spinner3);
        int var2=Integer.parseInt(String.valueOf(l1.getSelectedItem()));
        Spinner l2 = (Spinner) findViewById(R.id.spinner);
        int var1=Integer.parseInt(String.valueOf(l2.getSelectedItem()));
        int sum = var1-var2;

        wynik.setText(Integer.toString(sum));
    }
    public void iloczyn(View view){
        TextView wynik = (TextView) findViewById(R.id.textView6);
        Spinner l1 = (Spinner) findViewById(R.id.spinner3);
        int var2=Integer.parseInt(String.valueOf(l1.getSelectedItem()));
        Spinner l2 = (Spinner) findViewById(R.id.spinner);
        int var1=Integer.parseInt(String.valueOf(l2.getSelectedItem()));
        int sum = var1*var2;

        wynik.setText(Integer.toString(sum));
    }
    public void iloraz(View view){
        TextView wynik = (TextView) findViewById(R.id.textView6);
        Spinner l1 = (Spinner) findViewById(R.id.spinner3);
        int var2=Integer.parseInt(String.valueOf(l1.getSelectedItem()));
        Spinner l2 = (Spinner) findViewById(R.id.spinner);
        int var1=Integer.parseInt(String.valueOf(l2.getSelectedItem()));
        int sum;
        if(var1!=0&&var2!=0) {
            sum = var1 / var2;
        }else{
            sum =0;
        }
        wynik.setText(Integer.toString(sum));
    }
}
