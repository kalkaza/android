package com.example.lab7;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

public class DrinkActivity extends AppCompatActivity {

    public static final String EXTRA_DRINK0 = "drinkNo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink);

        int drinkNo = (Integer) getIntent().getExtras().get(EXTRA_DRINK0);
        try{
            SQLiteOpenHelper sqLiteOpenHelper =new DrinkSQLHelper(this);
            SQLiteDatabase db = sqLiteOpenHelper.getReadableDatabase();
            Cursor cursor = db.query("DRINK",new String[]{"NAME","DESCR"},"_id=?",
                    new String[]{Integer.toString(drinkNo)},null,null,null);
            if(cursor.moveToFirst()){
                String nameText = cursor.getString(0);
                String descText = cursor.getString(1);

                TextView nameField = (TextView) findViewById(R.id.name);
                nameField.setText(nameText);

                TextView descField = (TextView) findViewById(R.id.desc);
                descField.setText(descText);


            }
            cursor.close();
            db.close();
            cursor.close();
        }catch (SQLException e){
            Toast toast = Toast.makeText(this,"baza niedostepna",Toast.LENGTH_SHORT);
            toast.show();
        }

    }
}
