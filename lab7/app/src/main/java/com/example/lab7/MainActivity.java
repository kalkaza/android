package com.example.lab7;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private SQLiteDatabase db;
    private Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AdapterView.OnItemClickListener adapterView =new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position==0){
                    Intent intent = new Intent(MainActivity.this,
                            FoodCategoryActivity.class);
                    startActivity(intent);
                }
                if(position==1){
                    Intent intent = new Intent(MainActivity.this,
                            DrinkCategoryActivity.class);
                    startActivity(intent);
                }
            }
        };
        ListView listView = (ListView) findViewById(R.id.optionList);
        listView.setOnItemClickListener(adapterView);

        ListView listView1 = (ListView) findViewById(R.id.list_favorites);
        try {
            SQLiteOpenHelper sqLiteOpenHelper = new FoodSQLHelper(this);
            db = sqLiteOpenHelper.getReadableDatabase();
            cursor = db.query("FOOD", new  String[]{"_id","NAME"},
                    "FAVORITE = 1",null, null,null,null);
            CursorAdapter favAdapter = new SimpleCursorAdapter(MainActivity.this,
                    android.R.layout.simple_expandable_list_item_1,cursor,
                    new String[]{"NAME"},new int[]{android.R.id.text1});
            listView1.setAdapter(favAdapter);
        }catch (SQLException e){
            Toast toast = Toast.makeText(this,"Baza jest niedostępna",Toast.LENGTH_SHORT);
            toast.show();
        }

        listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, FoodActivity.class);
                intent.putExtra(FoodActivity.EXTRA_FOOD0,(int)id);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cursor.close();
        db.close();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        try {

            FoodSQLHelper sqlHelper = new FoodSQLHelper(this);
            db = sqlHelper.getReadableDatabase();
            Cursor newCursor = db.query("FOOD",
                    new String[] { "_id", "NAME"},
                    "FAVORITE = 1",
                    null, null, null, null);

            ListView listFavorites = (ListView)findViewById(R.id.list_favorites);
            CursorAdapter adapter = (CursorAdapter) listFavorites.getAdapter();

            adapter.changeCursor(newCursor);
            cursor= newCursor;
        } catch(SQLiteException e) {
            Toast toast = Toast.makeText(this, "Baza danych jest niedostępna",
                    Toast.LENGTH_SHORT);
            toast.show();
        }
    }


}
