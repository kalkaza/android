package com.example.lab7;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

public class FoodActivity extends AppCompatActivity {

    public static final String EXTRA_FOOD0 = "foodNo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food);

    int foodNo = (Integer) getIntent().getExtras().get(EXTRA_FOOD0);
    try{
        SQLiteOpenHelper sqLiteOpenHelper =new FoodSQLHelper(this);
        SQLiteDatabase db = sqLiteOpenHelper.getReadableDatabase();
        Cursor cursor = db.query("FOOD",new String[]{"NAME","DESCR","FAVORITE"},"_id=?",
                new String[]{Integer.toString(foodNo)},null,null,null);
        if(cursor.moveToFirst()){
            String nameText = cursor.getString(0);
            String descText = cursor.getString(1);
            boolean isFavorite = (cursor.getInt(3)==1);

            TextView nameField = (TextView) findViewById(R.id.name);
            nameField.setText(nameText);

            TextView descField = (TextView) findViewById(R.id.desc);
            descField.setText(descText);

            CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox);
            checkBox.setChecked(isFavorite);
        }
        db.close();
        cursor.close();
    }
    catch (SQLException e){
        Toast toast = Toast.makeText(this,"baza niedostepna",Toast.LENGTH_SHORT);
        toast.show();
        }
    }

    public void onFavClicked(View view){
        int foodNo = (Integer) getIntent().getExtras().get(EXTRA_FOOD0);
        new UpdateFoodTask().execute(foodNo);
    }

    private class UpdateFoodTask extends AsyncTask<Integer, Void, Boolean> {
        ContentValues contentValues;
        @Override
        protected void onPreExecute() {
            CheckBox fav = (CheckBox) findViewById(R.id.checkBox);
            contentValues = new ContentValues();
            contentValues.put("FAVORITE",fav.isChecked());
        }

        @Override
        protected Boolean doInBackground(Integer... food) {
            int foodNo= food [0];
            SQLiteOpenHelper sqLiteOpenHelper = new FoodSQLHelper(FoodActivity.this);
            try {
                SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();
                db.update("FOOD",contentValues,"_id=?",
                        new String[]{Integer.toString(foodNo)});
                db.close();
                return true;
            }
            catch (SQLException e){
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(!aBoolean){
                Toast toast = Toast.makeText(FoodActivity.this,"baza danych niedostępna"
                        ,Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }
}
